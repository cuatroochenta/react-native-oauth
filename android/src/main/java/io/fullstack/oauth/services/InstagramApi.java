package io.fullstack.oauth.services;

import com.github.scribejava.core.builder.api.DefaultApi20;
import com.github.scribejava.core.extractors.OAuth2AccessTokenExtractor;
import com.github.scribejava.core.extractors.TokenExtractor;
import com.github.scribejava.core.model.OAuth2AccessToken;
import com.github.scribejava.core.model.Verb;

public class InstagramApi extends DefaultApi20 {

    protected InstagramApi() {
    }

    private static class InstanceHolder {
        private static final InstagramApi INSTANCE = new InstagramApi();
    }

    public static InstagramApi instance() {
        return InstanceHolder.INSTANCE;
    }

   

    @Override
    public Verb getAccessTokenVerb() {
        return Verb.POST;
    }

    @Override
    public String getAccessTokenEndpoint() {
        return "https://api.instagram.com/oauth/access_token";
    }

    @Override
    protected String getAuthorizationBaseUrl() {
        return "https://api.instagram.com/oauth/authorize";
    }


}